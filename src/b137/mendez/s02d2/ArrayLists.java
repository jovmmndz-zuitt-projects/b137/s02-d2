package b137.mendez.s02d2;

import java.util.ArrayList;

public class ArrayLists {

    public static void main(String[] args) {
        System.out.println("ArrayLists\n");

        // Syntax
        // ArrayList<data_type> <identifier> = new ArrayList<data_type>();

        ArrayList<String> theBeatles = new ArrayList<String>();

        theBeatles.add("John");
        System.out.println(theBeatles.get(0));

        theBeatles.add("Paul");
        System.out.println(theBeatles);

        // Updating item(s) of an array list
        theBeatles.set(0, "John Lennon");
        System.out.println(theBeatles.get(0));

        //Removing an existing item in an array list
        theBeatles.add("Steph Curry");
        System.out.println(theBeatles);
        theBeatles.remove(2);
        System.out.println(theBeatles);

        //Removing a
        theBeatles.clear();
        System.out.println(theBeatles);
    }
}

